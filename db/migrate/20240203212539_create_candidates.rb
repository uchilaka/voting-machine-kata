# frozen_string_literal: true

class CreateCandidates < ActiveRecord::Migration[7.0]
  def change
    create_table :candidates, force: true, id: false do |t|
      t.primary_key :id, :string
      t.string :full_name, null: false

      t.timestamps
    end
  end
end
