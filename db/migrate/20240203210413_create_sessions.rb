# frozen_string_literal: true

# 20240203210413_create_sessions.rb
class CreateSessions < ActiveRecord::Migration[7.0]
  def change
    create_table :sessions, force: true, id: false do |t|
      t.primary_key :id, :string
      t.belongs_to :user, null: false, foreign_key: true, type: :string
      t.string :ip_address
      t.integer :time_to_live_sec, null: false

      t.timestamps
    end
  end
end
