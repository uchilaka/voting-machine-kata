# frozen_string_literal: true

require 'rails_helper'
require 'active_support/testing/time_helpers'

describe 'Time restricted session', type: :feature do
  before(:all) do
    self.accessibility_audit_enabled = true
  end

  include ActiveSupport::Testing::TimeHelpers

  around do |example|
    travel_to Time.zone.local(2024, 2, 5, 7, 0, 0)
    example.run
    travel_back
  end

  let(:email) { Faker::Internet.email }
  let(:postal_code) { Faker::Address.postcode }

  context 'when the session is idle for over 5 minutes' do
    it 'expires the current session after 5 minutes' do
      visit '/sessions/new'
      fill_in 'Email', with: email
      fill_in 'Postal code', with: postal_code
      click_on 'Continue to vote'
      expect(page).to have_current_path('/votes/new')
      expect(page).to have_content('Cast your vote today!')
      visit '/votes'
      # Advance time by 5 minutes and 1 second
      travel_to Time.zone.local(2024, 2, 5, 7, 5, 1)
      # Attempt to visit a view that requires a session
      visit '/votes/new'
      # Assert that the user is redirected to the login page
      expect(page).to have_current_path('/sessions/new')
    end
  end
end
