# frozen_string_literal: true

# == Schema Information
#
# Table name: votes
#
#  id           :string           not null, primary key
#  candidate_id :string           not null
#  user_id      :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
Fabricator(:vote) do
  candidate { Fabricate(:candidate) }
  user { Fabricate(:user) }
end
