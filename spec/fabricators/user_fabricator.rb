# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id          :string           not null, primary key
#  email       :string           not null
#  postal_code :string           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
Fabricator(:user) do
  email       { Faker::Internet.email }
  postal_code { Faker::Address.postcode }
end
