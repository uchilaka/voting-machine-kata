# frozen_string_literal: true

# == Schema Information
#
# Table name: candidates
#
#  id         :string           not null, primary key
#  full_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
Fabricator(:candidate) do
  full_name { Faker::Name.name }
end
