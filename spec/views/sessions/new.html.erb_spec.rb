# frozen_string_literal: true

require 'rails_helper'

describe.skip 'sessions/new', type: :view do
  before(:each) do
    assign(:session, Session.new(
                       time_to_live_sec: 1,
                       ip_address: 'MyString'
                     ))
  end

  it 'renders new session form' do
    render

    assert_select 'form[action=?][method=?]', sessions_path, 'post' do
      assert_select 'input[name=?]', 'session[time_to_live_sec]'

      assert_select 'input[name=?]', 'session[ip_address]'
    end
  end
end
