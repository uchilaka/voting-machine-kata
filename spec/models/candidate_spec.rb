# frozen_string_literal: true

# == Schema Information
#
# Table name: candidates
#
#  id         :string           not null, primary key
#  full_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

RSpec.describe Candidate, type: :model do
  describe '#cannot_exceed_ballot_limit' do
    before do
      (Candidate::BALLOT_LIMIT - 1).times { Fabricate :candidate }
    end

    it "fails validation on the #{(Candidate::BALLOT_LIMIT + 1).ordinalize} record" do
      last_candidate = Fabricate.build :candidate
      expect(last_candidate).to be_valid
      last_candidate.save

      next_candidate = Fabricate.build :candidate
      expect(next_candidate).to be_invalid
      expect(next_candidate.errors[:base]).to include("You cannot have more than #{Candidate::BALLOT_LIMIT} candidates")
    end
  end

  describe '#full_name' do
    it 'is required' do
      candidate = Fabricate.build :candidate, full_name: nil
      expect(candidate).to be_invalid
      expect(candidate.errors[:full_name]).to include("can't be blank")
    end

    it 'is unique' do
      Fabricate :candidate, full_name: 'John Doe'
      duplicate_candidate = Fabricate.build :candidate, full_name: 'john doe'
      expect(duplicate_candidate).to be_invalid
      expect(duplicate_candidate.errors[:full_name]).to include('has already been taken')
    end
  end
end
