# frozen_string_literal: true

# == Schema Information
#
# Table name: votes
#
#  id           :string           not null, primary key
#  candidate_id :string           not null
#  user_id      :string           not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
require 'rails_helper'

RSpec.describe Vote, type: :model do
  let(:voter) { Fabricate :user }

  before do
    Fabricate :candidate, full_name: 'John Doe'
    Fabricate :candidate, full_name: 'Jane Saunders'
  end

  it 'allows a user to vote for only 1 candidate' do
    vote = Vote.new(candidate: Candidate.find_by(full_name: 'John Doe'), user: voter)
    expect(vote).to be_valid
    vote.save

    duplicate_vote = Vote.new(candidate: Candidate.find_by(full_name: 'Jane Saunders'), user: voter)
    expect(duplicate_vote).to be_invalid
    expect(duplicate_vote.errors[:base]).to include(I18n.t('votes.ballot_already_voted'))
  end
end
