# Voting Machine App

A simple voting machine application built with Ruby on Rails.

## Project requirements

Before you start, ensure that you have the following installed on your system:
- NodeJS version 18.19.0 or later (edit the contents of `.node-version` to match your installed version)
- Ruby version 3.2.2

## Setup

To setup and run the app for the first time, run the following command:

```shell
./bin/setup
```

### Notes on using `asdf`

Ensure that you have the following configuration in your `~/.asdfrc` file.

```shell
legacy_version_file = yes
```

## Running the application

To run all component processes of the application, use the following command:

```shell
./bin/dev
```

## Features

- [x] Your Rails application will accept an email address and zip code on a sign-in form.
- [x] It will present the current list of candidates and the ability to add a candidate.
- [x] Adding a candidate will automatically cast a vote for that candidate.
- [x] It will show the current results tabulated per candidate on a separate, unauthenticated dashboard that updates once every 10 votes.

## Requirements

- [x] A given email address can vote 1 time.
- [x] There are no predetermined candidates, all votes will come in the form of write-ins. A write-in vote creates a new candidate on the form for the following voters to choose.
- [x] There is a maximum of 10 unique candidates.
- [x] Everyone gets exactly 5 minutes to vote after signing in before they need to sign in again.
  > Implemented a 5 minute session timeout in the `ApplicationController` using `before_action :check_authentication!`
- [x] We use postgres, you are free to use SQLite as your database. Memcached and Redis can be used.
- [x] The application is accessible to screen readers and keyboard navigation.
  > Some basic testing with macOS VoiceOver and keyboard navigation was done.
- [x] The application can be built and run by issuing a single command.
  > To build the app, run `/bin/setup`
  
## Implementation decisions

### Auth system

Building an application IRL, I would consider using `Devise` for user authentication.
with the limited scope of this project, it sufficed to use the built in session feature of
rails which meets the encrypted cookie requirement. If the app persisted this approach to session 
management, I would review [the guide for securing rails apps](https://guides.rubyonrails.org/security.html#rotating-encrypted-and-signed-cookies-configurations)
when hardening it.

### SQLite 

> Keeping things simple

If I were to migrate to using `PostgreSQL` in this application, I would start by reviewing the `bin/rails db:system:change --help` command for the switch 
and I would implement a `docker-compose.yml` script to manage the database server.

### Caching

> Improving request & query performance at scale

As a concern, I didn't spend much time reviewing options for a store to use for caching. If I had to 
implement one, I would likely go with `Redis` as a cache store and include management of the instance 
in the `docker-compose.yml`.

### `SecureRandom.uuid` for generating default UUIDs in models

I settled on this approach as good enough for the exercise to adopt UUIDs in favor of integer sequence IDs. 
For the use of UUID as primary keys, there was a bit of a workaround needed since the `sqlite-ulid` gem 
didn't seem to support macOS M1. The `attribute` definition in the `ApplicationModel` was used to generate a UUID
as a workaround for issues with the database `ULID()` default not being supported in SQLite running on M1 architecture.

There was a felt trade-off here between the simplicity of setup with SQLite and the workarounds needed to 
support UUIDs as primary keys (which seems to have better built-in support for UUIDs with rails).

### ActionCable

This was definitely a rabbit-hole for me on the project. I reviewed the [ActionCable overview](https://guides.rubyonrails.org/action_cable_overview.html#what-is-action-cable-questionmark) 
and the [ActionCable testing guide](https://guides.rubyonrails.org/testing.html#testing-websockets) to get a sense of how to implement it.


## Extending this application 

Review the API for scaffolding a new resource.

```shell
rails g scaffold --help
```

## Managing credentials 

```shell
# For the credentials help menu
bin/rails credentials:help

# For managing credentials of a specified environment
EDITOR=nano bin/rails credentials:edit --environment <development|test|production>
```

## Extra Credit

- [ ] Account for things like omitted middle initials/names, typos, or any other reason why a good faith but inaccurate write-in entry could lead to problems.
- [ ] Use react-query on the front end.
- [ ] Use GraphQL on the backend.
- [ ] Have the dashboard update “live” as new values are counted - i.e. without triggering a periodic refresh of the page.
- [x] Add specs. We use RSpec.

## References 

- Web Content Accessibility Guidelines (WCAG) 2.1: https://www.w3.org/TR/WCAG21
- Using UUIDs with SQLite in Rails: https://fractaledmind.github.io/2023/09/22/enhancing-rails-sqlite-ulid-primary-keys/
  - SQLite ULID (pending macOS support): https://github.com/asg017/sqlite-ulid
  - Some more notes on UUIDs: https://medium.com/rubycademy/how-rails-generates-uuids-by-extending-the-digest-library-2672443c9266
- Tailwind CSS Application UI components: https://tailwindui.com/components#product-application-ui
- Rails caching: https://guides.rubyonrails.org/caching_with_rails.html
- ActionCable overview: https://guides.rubyonrails.org/action_cable_overview.html#what-is-action-cable-questionmark
- Setup JS bundling with Rails: https://github.com/rails/jsbundling-rails?tab=readme-ov-file#installation
- Flowbite 
  - Getting started with Rails: https://flowbite.com/docs/getting-started/rails/
  - Blocks: https://flowbite.com/blocks/
  - Navbar: https://flowbite.com/docs/components/navbar/

## Future research / "extra" Extra credit 😅

- [ ] How to mock session data in Rails request & feature specs
- [ ] Writing specs for ActionCable channels https://guides.rubyonrails.org/testing.html#testing-websockets
- [ ] Deeper dive into encrypted cookie (vs session) use cases
- [ ] Add `RSpec` tests for the `ActionCable` channels (the Rails docs include `Minitest` examples)
- [ ] Implement the entire test suite in `Minitest`
