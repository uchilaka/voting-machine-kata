# frozen_string_literal: true

Rails.application.config do |config|
  # Initialize the cookied session store
  config.session_store :cookie_store, key: '_voter_session'
  config.middleware.use ActionDispatch::Cookies
  config.middleware.use config.session_store, config.session_options
end
