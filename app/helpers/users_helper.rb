# frozen_string_literal: true

module UsersHelper
  def user_has_voted?
    return false unless @current_user

    @current_user.votes.any?
  end
end
