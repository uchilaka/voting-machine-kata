import consumer from "./consumer"

consumer.subscriptions.create("ElectionChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    console.debug('ElectionChannel connected');
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
    console.warn('ElectionChannel disconnected');
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
    const { ballots_cast } = data
    console.debug({ ballots_cast });
    // Re-broadcast this message
    this.send(data)
    // Reload the page every 10 ballots
    if (ballots_cast % 10 === 0) window.location.reload();
    else { console.debug(`Will reload in ${10 - (ballots_cast % 10)} ballots`) }
  }
});
