# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = verified_user
    end

    private

    def verified_user
      @verified_user ||= User.find_by_id(user_id_from_session)
    end

    def user_id_from_session
      session_cookie = cookies.encrypted['_voter_session']
      return nil if session_cookie.nil?

      session_cookie['user.id']
    end
  end
end
